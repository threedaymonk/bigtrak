# Microcontroller code for robot

## Usage

Build and flash to an Arduino at `/dev/arduino`:

```sh
$ make
```

## Prerequisites

On Ubuntu 12.04:

```sh
$ sudo apt-get install make binutils-avr avr-libc avrdude gcc-avr
```
