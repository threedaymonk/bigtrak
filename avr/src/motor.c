#include <avr/io.h>
#include <stdlib.h>
#include "motor.h"

#define LATCH_PORT PORTB
#define LATCH_DDR DDRB
#define LATCH 4

#define CLK_PORT PORTD
#define CLK_DDR DDRD
#define CLK 4

#define ENABLE_PORT PORTD
#define ENABLE_DDR DDRD
#define ENABLE 7

#define SER_PORT PORTB
#define SER_DDR DDRB
#define SER 0

#define MOTOR12_64KHZ (1 << CS20) // no prescale

#define MOTOR1_A 2
#define MOTOR1_B 3
#define MOTOR2_A 1
#define MOTOR2_B 4

#define PWM1 OCR2A
#define PWM2 OCR2B

#define MOTOR_FORWARD  1
#define MOTOR_BACKWARD 2
#define MOTOR_RELEASE  4

static uint8_t current_directions[2] = {MOTOR_RELEASE, MOTOR_RELEASE};
static uint8_t latch_state;

#define latch_high(bit) (latch_state |=  (1 << bit))
#define latch_low(bit)  (latch_state &= ~(1 << bit))

static void latch_tx(void) {
  uint8_t i;

  LATCH_PORT &= ~(1 << LATCH);
  SER_PORT &= ~(1 << SER);

  for (i = 0; i < 8; i++) {
    CLK_PORT &= ~(1 << CLK);

    if (latch_state & (1 << (7 - i))) {
      SER_PORT |= (1 << SER);
    } else {
      SER_PORT &= ~(1 << SER);
    }
    CLK_PORT |= (1 << CLK);
  }
  LATCH_PORT |= (1 << LATCH);
}

static void mc_enable(void) {
  LATCH_DDR |= (1 << LATCH);
  ENABLE_DDR |= (1 << ENABLE);
  CLK_DDR |= (1 << CLK);
  SER_DDR |= (1 << SER);

  latch_state = 0;
  latch_tx(); // "reset"

  ENABLE_PORT &= ~(1 << ENABLE);
}

static inline void init_pwm_1(uint8_t freq) {
  // use PWM from timer2A on PB3 (Arduino pin #11)
  TCCR2A |= (1 << COM2A1) | (1 << WGM20) | (1 << WGM21); // fast PWM, turn on oc2a
  TCCR2B = freq & 0x7;
  PWM1 = 0;
  DDRB |= (1 << 3); // set pin to output mode
}

static inline void init_pwm_2(uint8_t freq) {
  // use PWM from timer2B on PD3 (pin 3)
  TCCR2A |= (1 << COM2B1) | (1 << WGM20) | (1 << WGM21); // fast PWM, turn on oc2b
  TCCR2B = freq & 0x7;
  PWM2 = 0;
  DDRD |= (1 << 3); // set pin to output mode
}

void set_direction(uint8_t num, uint8_t cmd) {
  uint8_t a, b;
  switch (num) {
  case 1:
    a = MOTOR1_A; b = MOTOR1_B; break;
  case 2:
    a = MOTOR2_A; b = MOTOR2_B; break;
  default:
    return;
  }

  switch (cmd) {
  case MOTOR_FORWARD:
    latch_high(a);
    latch_low(b);
    latch_tx();
    break;
  case MOTOR_BACKWARD:
    latch_low(a);
    latch_high(b);
    latch_tx();
    break;
  case MOTOR_RELEASE:
    latch_low(a);
    latch_low(b);
    latch_tx();
    break;
  }
}

void set_pwm(uint8_t num, uint8_t speed) {
  switch (num) {
  case 1:
    PWM1 = speed; break;
  case 2:
    PWM2 = speed; break;
  }
}

// PUBLIC API

void motor_init() {
  mc_enable();
  set_direction(1, MOTOR_RELEASE);
  set_direction(2, MOTOR_RELEASE);
  init_pwm_1(MOTOR12_64KHZ);
  init_pwm_2(MOTOR12_64KHZ);
}

// Set the current speed of a motor
// +ve values for forward, -ve for reverse, 0 for stop
// speed should be between -255 and 255
void motor_set_speed(uint8_t num, int16_t speed) {
  uint8_t new_direction;

  if (speed == 0) {
    new_direction = MOTOR_RELEASE;
  } else if (speed > 0) {
    new_direction = MOTOR_FORWARD;
  } else {
    new_direction = MOTOR_BACKWARD;
  }

  if (current_directions[num - 1] != new_direction) {
    set_direction(num, new_direction);
    current_directions[num - 1] = new_direction;
  }
  set_pwm(num, abs(speed));
}
