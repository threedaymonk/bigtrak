#ifndef __usart_h__
#define __usart_h__

void USART_init(uint32_t baudrate);
void USART_send(char c);
char USART_receive(void);
void USART_send_string(char *s);

#endif
