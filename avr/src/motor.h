#ifndef __motor_h__
#define __motor_h__

#include <inttypes.h>

void motor_init();
void motor_set_speed(uint8_t num, int16_t speed);

#endif
