#include <avr/io.h>

void USART_init(uint32_t baudrate) {
  uint16_t baud_prescaler = ((F_CPU / (baudrate * 16UL))) - 1;
  UBRR0H = (uint8_t)(baud_prescaler >> 8);
  UBRR0L = (uint8_t)(baud_prescaler & 0xFF);
  UCSR0B = (1 << RXEN0) | (1 << TXEN0);
  UCSR0C = (1 << UCSZ00) | (1 << UCSZ01);
}

void USART_send(char c) {
  while (!(UCSR0A & (1 << UDRE0)));
  UDR0 = c;
}

char USART_receive(void) {
  while (!(UCSR0A & (1 << RXC0)));
  return UDR0;
}

void USART_send_string(char *s) {
  while (*s) USART_send(*(s++));
}
