#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdint.h>
#include <stdio.h>

#include "motor.h"
#include "usart.h"

#define MAX_SPEED 140

void go(int left, int right) {
  char buffer[256];
  snprintf(buffer, 256, "Left: %d, Right: %d\r\n", (left * MAX_SPEED) / 10, (right * MAX_SPEED) / 10);
  USART_send_string(buffer);

  motor_set_speed(1, (left  * MAX_SPEED) / 10);
  motor_set_speed(2, (right * MAX_SPEED) / 10);
}

void loop(void) {
  char received;
  char buffer[256];
  int8_t left = 0, right = 0, speed = 0;

  while(1) {
    received = USART_receive();

    snprintf(buffer, 256, "Received: \c %02X\r\n", received, received);
    USART_send_string(buffer);

    if (received >= '1' && received <= '9') {
      speed = received - '0';
    } else switch (received) {
      case 'w': left =  1; right =  1; break;
      case 'a': left =  1; right = -1; break;
      case 's': left = -1; right = -1; break;
      case 'd': left = -1; right =  1; break;
      case ' ': left =  0; right =  0; break;
      default: return;
    }
    go(left * speed, right * speed);
  }
}

int main(void) {
  cli();
  motor_init();
  USART_init(2400);
  USART_send_string("I AM A BOT\r\n");
  loop();
}
