Set up TL-MR3020 as robot controller
====================================

Install OpenWRT.

Connect via crossover cable.

Do normal first boot stuff (set password):

```sh
passwd
```

## LAN setup

Change `/etc/config/network` lan configuration to:

    config interface 'lan'
            option ifname 'eth0'
            option type 'bridge'
            option proto 'dhcp'

Connect to network with DHCP.

Reboot.

Use nmap to find it again.

SSH back in.

## External storage

Format USB stick with:

```sh
sudo mkfs.ext4 -O ^has_journal /dev/TODO
```

Install USB storage support:

```sh
opkg update
opkg install kmod-usb-storage kmod-fs-ext4 kmod-scsi-generic block-mount
mkdir /mnt/sda1
mount /dev/sda1 /mnt/sda1
```

Edit /etc/config/fstab to include:

    config mount
            option target   /overlay
            option device   /dev/sda1
            option fstype   ext4
            option options  rw,sync,noatime
            option enabled  1
            option enabled_fsck 0

Copy old overlay:

```sh
tar -C /overlay -cvf - . | tar -C /mnt/sda1 -xf -
```

Reboot

```sh
reboot
```

## Avahi

```sh
opkg update
opkg install dbus avahi-daemon
```

Edit `/etc/dbus-1/system.d/avahi-dbus.conf`. Comment out the netdev part
(there's no netdev group).

```sh
/etc/init.d/dbus enable
/etc/init.d/avahi-daemon enable
```

Set hostname in `/etc/config/system`.

Copy public key to `/etc/dropbear/authorized_keys`.

Reboot.

Can now SSH to `root@<hostname>.local`.

## Webcam

On another Linux machine, use `tail -f /var/log.syslog` to find webcam driver.
Then follow http://wiki.openwrt.org/doc/howto/usb.video
E.g.

```sh
opkg update
opkg install kmod-video-gspca-core kmod-video-gspca-stv06xx
opkg install fswebcam
```

## Access point


Edit `/etc/config/network` and define a new interface section:

    config interface 'wifi'
            option proto 'static'
            option ipaddr '192.168.2.1'
            option netmask '255.255.255.0'

In `/etc/config/wireless`, enable the wireless:

    config wifi-device  radio0
            ...
            option disabled 0

Locate the existing wifi-iface section and change its
network option to point to the newly created interface section.

    config wifi-iface
            option device     radio0
            option network    wifi
            option mode       ap
            option ssid       bigtrak
            option encryption psk2
            option key        'TODO: passphrase goes here'

Enable DHCP by adding to `/etc/config/dhcp`:

    config dhcp wifi
            option interface  wifi
            option start      100
            option limit      150
            option leasetime  12h

Set everything to work:

```sh
ifup wifi
wifi
/etc/init.d/dnsmasq restart
```

Can now connect to the access point.

## Minicom and USB serial

```sh
opkg update
opkg install minicom kmod-usb-serial kmod-usb-acm
```

Set up minicom

```sh
TERM=xterm minicom -o -s
```
